# Whitelist
Filter who can enter a Minetest server and who can't through a whitelist

<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>  

### Commands
`/whitelist on`: enables the whitelist  
`/whitelist off`: disables the whitelist  
`/whitelist add <player>`: adds <player> to the whitelist (not case sensitive)  
`/whitelist remove <player>`: removes <player> from the whitelist (not case sensitive)  
`/whitelist who`: lists all the whitelisted players

### API
Have a look at the [DOCS](https://gitlab.com/zughy-friends-minetest/whitelist/-/blob/master/DOCS.md)

### Want to help?
Feel free to:
* open an [issue](https://gitlab.com/zughy-friends-minetest/whitelist/-/issues)
* check [Weblate](https://translate.codeberg.org/projects/zughy-friends-minetest/whitelist/) for translations
* submit a merge request. In this case, PLEASE, do follow milestones and my [coding guidelines](https://cryptpad.fr/pad/#/2/pad/view/-l75iHl3x54py20u2Y5OSAX4iruQBdeQXcO7PGTtGew/embed/)